# Copyright 2008, 2009 Mike Kelly
# Distributed under the terms of the GNU General Public License v2

require pam \
    autotools [ supported_autoconf=[ 2.7 ]  supported_automake=[ 1.16 ] ]
require ffmpeg [ with_opt=true option_name=gtk ]

SUMMARY="Modular X screen saver and locker"
DESCRIPTION="
XScreenSaver is the standard screen saver collection shipped on most
Linux and Unix systems running the X11 Window System.
"
HOMEPAGE="https://www.jwz.org/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/changelog.html"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/faq.html"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk [[ description = [ Build the preferences GUI and some additional screen savers ] ]]

    ( libc: musl )
    ( providers: elogind systemd ) [[ number-selected = at-most-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( linguas: da de es et fi fr hu it ja ko nb nl pl pt pt_BR ru sk sv vi wa zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config
        x11-proto/xorgproto
    build+run:
        dev-libs/at-spi2-core[>=2.52.0]
        dev-libs/libglvnd
        dev-libs/libxml2:2.0[>=2.4.6]
        media-libs/libpng:=
        sys-apps/bc
        sys-libs/pam
        x11-apps/xdg-utils
        x11-dri/glu
        x11-dri/mesa
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libXext
        x11-libs/libXft[>=2.1.0]
        x11-libs/libXi
        x11-libs/libXinerama
        x11-libs/libXmu
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXt
        x11-libs/libXxf86vm
        gtk? (
            dev-libs/glib:2
            x11-libs/gdk-pixbuf:2.0[X]
            x11-libs/gtk+:3[>=2.22]
            x11-libs/pango
        )
        !libc:musl? ( dev-libs/libxcrypt:= )
        providers:elogind? ( sys-auth/elogind )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:systemd? ( sys-apps/systemd[>=221] )
    recommendation:
        x11-apps/appres [[ description = [ needed for xscreensaver-text ] ]]
"

WORK="${WORKBASE}"/${PN}-$(ever range -2)

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-Don-t-return-a-non-zero-exit-code-when-warning.patch
    "${FILES}"/${PN}-6.07-0004-ffmpeg-out.c-include-additional-header-file.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --hates=disable-dependency-tracking
    --hates=disable-silent-rules
    --hates=enable-fast-install
    --enable-locking
    --enable-nls
    --disable-root-passwd
    --with-dpms-ext
    --with-browser=xdg-open
    --with-jpeg
    --with-png
    --with-proc-oom
    --with-pthread
    --with-randr-ext
    --with-app-defaults=/usr/share/X11/app-defaults
    --with-xdbe-ext
    --with-xf86gamma-ext
    --with-xf86vmode-ext
    --with-xft
    --with-xinerama-ext
    --with-xinput-ext
    --with-xshm-ext
    --without-gle
    --without-gles
    --without-glx
    --without-kerberos
    --without-setuid-hacks
    --without-sgi-ext
    --without-sgivc-ext
    --without-motif
    --without-xidle-ext
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    gtk
    'gtk pixbuf'
    'gtk record-animation'
    'providers:elogind elogind'
    'providers:systemd systemd'
)

src_prepare() {
    # do not try to install non-existent file
    if ! option gtk ; then
        edo sed \
            -e 's:command xscreensaver-settings:command:g' \
            -i driver/Makefile.in
    fi

    autotools_src_prepare
}

src_install() {
    emake install_prefix="${IMAGE}" GTK_DATADIR=/usr/share PO_DATADIR=/usr/share install

    pamd_mimic_system "${PN}" auth auth

    # do not install a symlink to a non-existent binary
    ! option gtk && edo rm "${IMAGE}"/usr/$(exhost --target)/bin/${PN}-demo
    # do not install man pages for nonexistent binaries
    ! option gtk && edo rm "${IMAGE}"/usr/share/man/man1/${PN}-demo.1
}

