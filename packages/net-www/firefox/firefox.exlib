# Copyright 2007-2008 Alexander Færøy <eroyf@exherbo.org>
# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2011, 2012, 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PV="${MY_PV:-${PV/_beta/b}}"
MY_PV="${MY_PV/_rc/rc}"

# /mozilla-*/browser/branding/unofficial/locales/en-US/brand.properties
MOZ_CODENAME="Mozilla Developer Preview"

# firefox-${PV}/browser/locales/shipped-locales
linguas=(
    ach af an ar ast az be bg bn br bs ca_valencia ca cak cs cy da de dsb el
    en_CA en_GB en_US eo es_AR es_CL es_ES es_MX et eu fa ff fi fr fur fy_NL
    ga_IE gd gl gn gu_IN he hi_IN hr hsb hu hy_AM ia id is it ja ka kab kk km
    kn ko lij lt lv mk mr ms my nb_NO ne_NP nl nn_NO oc pa_IN pl pt_BR pt_PT
    rm ro ru sat sc sco si sk skr sl son sq sr sv_SE ta te tg th tl tr trs uk
    ur uz vi xh zh_CN zh_TW
)

require mozilla-pgo [ co_project=browser codename="${MOZ_CODENAME}" ]
require llvm [ additional_packages=[ "dev-lang/clang" ] ]
require freedesktop-desktop
require toolchain-funcs
require utf8-locale
require providers
require rust

export_exlib_phases pkg_setup src_unpack src_prepare src_configure src_compile src_install

SUMMARY="Mozilla's standalone web browser"
HOMEPAGE="https://www.mozilla.com/en-US/${PN}"
DOWNLOADS="
    https://ftp.mozilla.org/pub/${PN}/releases/${MY_PV}/source/${PN}-${MY_PV}.source.tar.xz
"
for lang in "${linguas[@]}" ; do
    DOWNLOADS+="
        linguas:${lang}? ( https://ftp.mozilla.org/pub/${PN}/releases/${MY_PV}/linux-i686/xpi/${lang/_/-}.xpi -> ${PN}-${MY_PV}-${lang}.xpi )
    "
done

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/${MY_PV}/releasenotes"

LICENCES="
    || ( MPL-1.1 MPL-2.0 GPL-2 GPL-3 LGPL-2.1 LGPL-3 )
"
SLOT="0"
MYOPTIONS="
    eme       [[ description = [ Enable Encrypted Media Extensions, a form of DRM used for sites like Netflix ] ]]
    jack      [[ description = [ Enable JACK backend ] ]]
    libproxy  [[ description = [ Use libproxy for system proxy settings ] ]]
    lto       [[ description = [ Enable cross-language (c++ + rust) link-time optimisations ] ]]
    pulseaudio
    wayland   [[ description = [ Use wayland as the default backend ] ]]

    ( platform: amd64 x86 )
    ( libc: musl )
    ( linguas: ${linguas[@]} )
    ( providers: jpeg-turbo )
"

DEPENDENCIES="
    build:
        dev-lang/perl:*[>=5.6]
        dev-lang/python:*[>=3.6][sqlite]
        dev-lang/rust:*[>=1.47.0][llvm_abis:*(-)?]
        dev-rust/cbindgen[>=0.26.0]
        virtual/pkg-config[>=0.9.0]
        virtual/unzip
        lto? ( sys-devel/lld:* )
        platform:amd64? ( dev-lang/nasm[>=2.14] )
        platform:x86? ( dev-lang/nasm[>=2.14] )
    build+run:
        app-spell/hunspell:*
        dev-lang/node
        dev-libs/at-spi2-core[>=2.52.0]
        dev-libs/glib:2[>=2.42]
        dev-libs/icu:=[>=73.1]
        dev-libs/libevent:=
        dev-libs/libffi:=[>=3.0.10]
        dev-libs/libglvnd
        dev-libs/nspr[>=4.32]
        media-libs/fontconfig[>=2.7.0]
        media-libs/freetype:2[>=2.2.0] [[ note = [ aka 9.10.3 ] ]]
        media-libs/libvpx:=[>=1.10.0]
        media-libs/libwebp:=[>=1.0.2]
        sys-apps/dbus[>=0.60]
        sys-libs/zlib[>=1.2.3]
        sys-sound/alsa-lib
        x11-libs/cairo[>=1.10.2-r1][X] [[ note = [ required for tee backend ] ]]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3[>=3.14.0][wayland]
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXcomposite
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libxkbcommon[>=0.4.1]
        x11-libs/libXrandr[>=1.4.0]
        x11-libs/libXt
        x11-libs/pango[>=1.22.0]
        x11-libs/pixman:1[>=0.40.0]
        jack? ( media-sound/jack-audio-connection-kit )
        libproxy? ( net-libs/libproxy:1 )
        !pgo? ( dev-libs/nss[>=3.107] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        pulseaudio? ( media-sound/pulseaudio )
    suggestion:
        media/ffmpeg:*[<8] [[ description = [ Play patent-encumbered media formats ] ]]
        x11-libs/libnotify[>=0.4] [[ description = [ Show notifications with libnotify ] ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/firefox-rust-target-override.patch
)

MOZILLA_SRC_CONFIGURE_PARAMS=(
    --disable-crashreporter
    --disable-install-strip
    --disable-necko-wifi
    --disable-openmax
    --disable-snapshot-fuzzing
    --disable-strip
    --disable-synth-speechd # seems dependencyless but dlopens libspeechd.so.2
    --disable-updater
    --disable-warnings-as-errors
    --enable-chrome-format=omni
    --enable-dbus
    --enable-hardening
    --enable-optimize
    --enable-release
    --enable-rust-simd
    --enable-sandbox
    --enable-system-ffi
    --enable-system-pixman
    --enable-webrtc
    --with-intl-api
    --with-system-icu
    --with-system-libevent
    --with-system-libvpx
    --with-system-nspr
    --with-system-webp
    --with-system-zlib # USE_LIBZ_RS to use libz-rs-sys exists
    --without-system-png # Requires patches for APNG support, which we will not support
    --without-wasm-sandboxed-libraries
    # The build fails with --enable-tests (which is set by debug or pgo
    # and system nss: "x86_64-pc-linux-gnu-ld: error: cannot find -lnssutil3"
)

MOZILLA_SRC_CONFIGURE_OPTIONS=(
    'libc:musl --disable-jemalloc --enable-jemalloc'
    'debug --without-system-nss'
    'pgo --without-system-nss'
)

# See eme_choices in toolkit/moz.configure for the supported platforms.
case "$(exhost --target)" in
    x86_64-*|i686-*)
        MOZILLA_SRC_CONFIGURE_OPTIONS+=( 'eme --enable-eme=widevine --disable-eme' )
        ;;
esac

MOZILLA_SRC_CONFIGURE_OPTION_ENABLES=(
    libproxy
)
MOZILLA_SRC_CONFIGURE_OPTION_WITHS=(
    # --with-system-jpeg needs libjpeg-turbo or whatever provides JCS_EXTENSIONS
    'providers:jpeg-turbo system-jpeg'
)

firefox_pkg_setup() {
    require_utf8_locale

    default
}

firefox_src_unpack() {
    default

    for lang in "${linguas[@]}" ; do
        if option linguas:${lang} ; then
            edo mkdir "${WORKBASE}"/${lang}
            edo unzip -qo "${FETCHEDDIR}"/${PN}-${MY_PV}-${lang}.xpi -d "${WORKBASE}"/${lang}
        fi
    done
}

firefox_src_prepare() {
    mozilla-pgo_src_prepare

    edo sed -e "s/objdump/$(exhost --tool-prefix)objdump/" \
        -i python/mozbuild/mozbuild/configure/check_debug_ranges.py

    has_version "dev-libs/icu[>=76.1]" && \
        expatch "${FILES}"/firefox-icu-76.patch
}

firefox_src_configure() {
    llvm_providers_set

    # x86_64-pc-linux-gnu-as: invalid option -- 'N'
    export AS=$(exhost --tool-prefix)cc
    export MOZBUILD_STATE_PATH="${TEMP}".mozbuild
    # NOTE(somasis): fix library loading on musl
    # NOTE(woutershep): do not apply to glibc, breaks PGO.
    libc-is-musl && append-flags "-Wl,-rpath=/usr/$(exhost --target)/lib/${PN}"
    # Cross-language lto requires building C/C++ components with clang, and linking with lld
    optionq lto && providers_set 'cpp clang' 'cc clang' 'c++ clang' 'ld lld'
    # NOTE(mixi): Override the rust targets to our values.
    export MOZ_RUST_HOST=$(rust_build_arch_name)
    export MOZ_RUST_TARGET=$(rust_target_arch_name)

    local audio_backends="alsa"
    option jack && audio_backends+=",jack"
    option pulseaudio && audio_backends+=",pulseaudio"

    # Disable elf-hack for now because of https://bugzilla.mozilla.org/show_bug.cgi?id=1482204
    mozilla-pgo_src_configure \
        $(cc-is-clang || echo "--with-clang-path=/usr/$(exhost --build)/bin/$(exhost --tool-prefix)clang-$(llvm_get_abi)") \
        $(ld-is-lld && echo "--disable-elf-hack") \
        $(option lto && echo "--enable-lto=cross") \
        --enable-audio-backends=${audio_backends} \
        --enable-default-toolkit=cairo-gtk3$(option wayland -wayland)
    # NOTE: We could pass cairo-gtk3-wayland-only to avoid the X11 deps, but
    #       webrtc depends on X11 for now, which is probably more important
}

firefox_src_compile() {
    esandbox allow_net "unix:${TEMP}/pymp-*/listener-*"

    mozilla-pgo_src_compile

    esandbox disallow_net "unix:${TEMP}/pymp-*/listener-*"
}

firefox_src_install() {
    mozilla-pgo_src_install

    # some resources (ex: jsloader, jssubloader) are put into ommi-jar so the directory is empty
    edo find "${IMAGE}" -type d -empty -delete

    # allow installation of distributed extensions and read/use system locale on runtime
    insinto /usr/$(exhost --target)/lib/${PN}/browser/defaults/preferences
    hereins all-exherbo.js <<EOF
pref("extensions.autoDisableScopes", 3);
pref("intl.locale.requested", "");
EOF

    for lang in "${linguas[@]}" ; do
        if option linguas:${lang} ; then
            # Extract an id from the localisation tarball, usually something
            # like langpack-${lang}@firefox.mozilla.org. Not entirely sure
            # which rules it follows so I go with cargo-cult for now.
            emid="$(sed -n -e 's/.*"id": "\(.*\)",/\1/p' "${WORKBASE}"/${lang}/manifest.json | xargs)"
            insinto /usr/$(exhost --target)/lib/${PN}/distribution/extensions
            newins "${FETCHEDDIR}"/${PN}-${MY_PV}-${lang}.xpi ${emid}.xpi
        fi
    done
}

