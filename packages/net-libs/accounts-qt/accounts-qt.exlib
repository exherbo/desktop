# Copyright 2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ user=accounts-sso pn=lib${PN} tag=VERSION_${PV} suffix=tar.bz2 new_download_scheme=true ]
if ever at_least 1.17 ; then
    require qmake [ slot=6 ]
else
    require qmake
fi
require test-dbus-daemon

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="Library which provides a storage solution for user accounts"
DESCRIPTION="
The API provides access to the account storage: retrieval, editing and
creation of accounts, as well as enumeration of the currently installed account
providers and services. A change notification mechanism is implemented in order
to let different processes operate on the same data simultaneously.
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc
"

# fails AccountsTest::testAccountService(), last checked: 1.16
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            app-doc/doxygen
        )
    build+run:
        dev-libs/glib:2
        net-libs/libaccounts-glib
"

if ever at_least 1.17 ; then
    DEPENDENCIES+="
        build:
            doc? ( x11-libs/qttools:6 [[ note = [ qhelpgenerator ] ]] )
        build+run:
            x11-libs/qtbase:6
    "
else
    DEPENDENCIES+="
        build:
            doc? ( x11-libs/qttools:5 [[ note = [ qhelpgenerator-qt5 ] ]] )
        build+run:
            x11-libs/qtbase:5
    "
fi

accounts-qt_src_prepare() {
    if ever at_least 1.17 ; then
        # fix filesdir locations in the pkg-config file
        edo sed \
            -e "/filesdir/s:=\${prefix}:=/usr:" \
            -i Accounts/${PN}6.pc.in

        # fix qhelpgenerator invocation
        edo sed \
            -e "s:qhelpgenerator:/usr/$(exhost --target)/lib/qt6/libexec/qhelpgenerator:g" \
            -i doc/doxy.conf
    else
        # fix filesdir locations in the pkg-config file
        edo sed \
            -e "/filesdir/s:=\${prefix}:=/usr:" \
            -i Accounts/${PN}5.pc.in

        # fix qhelpgenerator invocation
        edo sed \
            -e 's:qhelpgenerator:qhelpgenerator-qt5:g' \
            -i doc/doxy.conf
    fi

    edo sed \
        -e "s:\(system(\)pkg-config:\1${PKG_CONFIG}:" \
        -i tests/tst_libaccounts.pro

    default
}

accounts-qt_src_configure() {
    local qmake_params=(
        PREFIX=/usr/$(exhost --target)
        LIBDIR=/usr/$(exhost --target)/lib
    )

    option doc || edo sed -e "/include( doc\/doc.pri )/d" -i accounts-qt.pro

    if ever at_least 1.17 ; then
        eqmake 6 "${qmake_params[@]}"
    else
        eqmake 5 "${qmake_params[@]}"
    fi
}

accounts-qt_src_compile() {
    default

    option doc && emake docs
}

accounts-qt_src_install() {
    default

    if option doc ; then
        edo mv "${IMAGE}"/usr/$(exhost --target)/share/doc/${PN}/* \
               "${IMAGE}"/usr/share/doc/${PNVR}
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/share{/doc{/accounts-qt,},}
    fi
}

