# Copyright 2013 Paul Seidler
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix="https://gitlab.freedesktop.org" user=mobile-broadband suffix=tar.bz2 new_download_scheme=true $(ever is_scm && echo branch=main) ]
require bash-completion
require meson
require udev-rules

SUMMARY="Library for talking to devices with Qualcomm MSM Interface (QMI) protocol"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/${PN}"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    mbim [[ description = [ QMI over MBIM, required by recent qualcomm modems ] ]]
    qrtr [[ description = [ Enable support for QRTR (qualcomm) protocol ] ]]
"

# Wants to access /dev/virtual/qmi*
RESTRICT="test"

DEPENDENCIES="
    build:
        sys-apps/help2man
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/glib:2[>=2.56][gobject-introspection(+)?]
        gnome-desktop/libgudev[>=232]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        mbim? ( net-libs/libmbim[>=1.18.0] )
        qrtr? ( net-libs/libqrtr-glib[>=1.0.0] )
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dcollection=full
    -Dfirmware_update=true
    -Dmm_runtime_check=true
    -Drmnet=true
    -Dudev=true
    -Dudevdir="${UDEVDIR}"
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bash-completion bash_completion'
    'gobject-introspection introspection'
    'gtk-doc gtk_doc'
    'mbim mbim_qmux'
    'qrtr'
)

