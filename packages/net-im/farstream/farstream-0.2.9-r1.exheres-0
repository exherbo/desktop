# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="The Farstream VVoIP framework (formerly farsight2)"
HOMEPAGE="https://www.freedesktop.org/wiki/Software/Farstream"
DOWNLOADS="https://freedesktop.org/software/${PN}/releases/${PN}/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0.2"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
"

# TODO (compnerd) need to filter network access
RESTRICT="test"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.20]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.10.1] )
        gtk-doc? ( dev-doc/gtk-doc[>=1.18] )
    build+run:
        dev-libs/glib:2[>=2.40][gobject-introspection(+)?]
        media-libs/gstreamer:1.0[>=1.4][gobject-introspection?]
        media-plugins/gst-plugins-base:1.0[>=1.4]
        net-im/libnice[>=0.1.8]
        !net-im/farsight2:0.10 [[
            description = [ file collisions with deprecated farsight2 ]
            resolution = uninstall-blocked-after
        ]]
"

AT_M4DIR=( common/m4 )

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-build-Adapt-to-backwards-incompatible-change-in-GNU-.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-gupnp
    --disable-static-plugins
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
)

