# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require flag-o-matic

export_exlib_phases src_configure src_compile src_test src_install

SUMMARY="Netscape Portable Runtime"
DESCRIPTION="
Netscape Portable Runtime (NSPR) provides a platform-neutral API for system level and libc like functions.
The API is used in the Mozilla clients and many of Red Hat's, Sun's, and other software offerings.
"
HOMEPAGE="https://www.mozilla.org/projects/${PN}"
DOWNLOADS="https://ftp.mozilla.org/pub/${PN}/releases/v${PV}/src/${PNV}.tar.gz"

LICENCES="MPL-2.0"
SLOT="0"
MYOPTIONS="
    debug
    parts: development libraries
"

DEPENDENCIES=""

WORK=${WORKBASE}/${PNV}/nspr

nspr_src_configure() {
    local myconf=(
        # nspr doesn't follow the canonical autotools definition of $build, $host and $target
        # https://bugzilla.mozilla.org/show_bug.cgi?id=742033
        # nspr only respects $CC, $LD, etc if $host != $target, set a dummy $host to make that true
        # for native builds as well. afaict, $host isn't actually used for anything besides that
        # check and a building an install tool (see below)
        # TODO: maim upstream to make them always honour those
        --host=none-none-none
        --target=$(exhost --target)
        --enable-ipv6
        $(option_enable debug)
    )

    # Respect LDFLAGS
    export DSO_LDOPTS="${LDFLAGS}"

    # Respect flags for host compiler (only used to build config/nsinstall afaict)
    export HOST_CC="$(exhost --build)-cc"
    export HOST_CFLAGS="$(print-build-flags CFLAGS)"
    export HOST_LDFLAGS="$(print-build-flags LDFLAGS)"

    [[ $(exhost --target) =~ ^(x86_64|aarch64)- ]] && \
        myconf+=( --enable-64bit )

    # CPPFLAGS from the target leak into HOST_CFLAGS causing cross-compiling
    # to fail.
    exhost --is-native -q || unset CPPFLAGS

    # NOTE(somasis): _linux.h tests for GLIBC crap, this fixes musl
    export CFLAGS="${CFLAGS} -D_PR_POLL_AVAILABLE -D_PR_HAVE_LARGE_OFF_T -D_PR_INET6 -D_PR_HAVE_INET_NTOP -D_PR_HAVE_GETHOSTBYNAME2 -D_PR_HAVE_GETADDRINFO -D_PR_INET6_PROBE"
    econf "${myconf[@]}"
}

nspr_src_compile() {
    # used in Makefile
    unset TARGETS

    default
}

nspr_src_test() {
    edo perl "${WORK}"/pr/tests/runtests.pl
}

nspr_src_install() {
    # used in Makefile
    unset TARGETS

    default

    edo rm "${IMAGE}"/usr/$(exhost --target)/bin/{compile-et.pl,prerr.properties}

    expart libraries /usr/$(exhost --target)/lib
    expart development /usr/$(exhost --target)/bin/${PN}-config
    expart development /usr/$(exhost --target)/include
    expart development /usr/$(exhost --target)/lib/pkgconfig
    expart development /usr/$(exhost --target)/lib/*.a
    expart development /usr/share/aclocal
}

