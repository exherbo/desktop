# Copyright 2014 Marc-Antoine Perennou <Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

MY_PV="${PV%_p*}"
PATCH_VERSION="${PV#*_p}"
UBUNTU_RELEASE=20.10

require launchpad
require vala [ vala_dep=true with_opt=true option_name=vapi ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

SUMMARY="A library to allow applications to export a menu into the Unity Menu bar"
DOWNLOADS="http://archive.ubuntu.com/ubuntu/pool/main/liba/${PN}/${PN}_${MY_PV}+${UBUNTU_RELEASE}.${PATCH_VERSION}.1.orig.tar.gz"


LICENCES="LGPL-2.1 LGPL-3"
SLOT="0.1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gobject-introspection
    gtk-doc
    mono
"

# Broken, don't build
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-doc/gtk-doc[>=1.32-r1]
        virtual/pkg-config
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.6.7] )
    build+run:
        dev-libs/dbus-glib:1[>=0.82]
        dev-libs/glib:2[>=2.35.4][gobject-introspection(+)?]
        dev-libs/libdbusmenu:0.4[>=0.5.90]
        x11-libs/gtk+:3[>=2.91][gobject-introspection?]
        mono? ( gnome-bindings/gtk-sharp:2 )
"

WORK="${WORKBASE}"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-mono-test
    --disable-static
    --disable-tests
    --with-gtk=3
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
    'gtk-doc'
    'mono'
)

src_prepare() {
    # Mono can't be disabled with a switch so we require a fantasy version
    if ! option mono ; then
        edo sed -i -e "/MONO_REQUIRED_VERSION/s:1.0:99&:" configure.ac
    fi

    edo gtkdocize --copy
    autotools_src_prepare
}

