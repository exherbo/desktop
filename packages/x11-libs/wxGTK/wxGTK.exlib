# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_PN=wxWidgets
require github [ release=v${PV} suffix=tar.bz2 ] toolchain-funcs alternatives

myexparam supported_gtk=[ 2 3 ]
exparam -v SUPPORTED_GTK_VERSIONS supported_gtk[@]

export_exlib_phases src_prepare src_configure src_compile src_install pkg_preinst

SUMMARY="Cross-Platform GUI Library"
DESCRIPTION="
wxWidgets gives you a single, easy-to-use API for writing GUI applications on
multiple platforms that still utilize the native platform's controls and
utilities.
"
HOMEPAGE+=" http://www.wxwidgets.org"

LICENCES="LGPL-2"
SLOT="$(ever range 1-2)"
MYOPTIONS="doc"

# If more than one GTK version is supported we provide options to build them
# selectively, if instead only one version is supported we are always building it
if [[ ${#SUPPORTED_GTK_VERSIONS[@]} -gt 1 ]]; then
    MYOPTIONS+=" ( providers: "
    for GTK_VERSION in ${SUPPORTED_GTK_VERSIONS[@]}; do
        MYOPTIONS+="
            gtk${GTK_VERSION} [[ description = [ Provides a wxGTK version built against GTK+ ${GTK_VERSION} ] ]]
        "
    done
    MYOPTIONS+=" ) [[ number-selected = at-least-one ]]"
fi

WORK="${WORKBASE}"/${MY_PN}-${PV}

wxGTK_src_prepare() {
    default

    # don't install windows files
    edo sed -e '/^install: /s/locale_msw_install //' -i Makefile.in
}

configure_prepare_one_multibuild() {
    edo mkdir -p "${WORKBASE}/build/gtk${GTK_VERSION}/"
    export ECONF_SOURCE="${WORK}"
}

wxGTK_handle_alternatives() {
    local config_binary alternatives=()

    # handle alternatives
    edo pushd "${IMAGE}"
    edo rm usr/$(exhost --target)/bin/wx{rc,-config}
    alternatives+=( /usr/$(exhost --target)/bin/wxrc wxrc-${SLOT} )

    edo mv usr/share/aclocal/wxwin{,-${SLOT}}.m4
    alternatives+=( /usr/share/aclocal/wxwin.m4 wxwin-${SLOT}.m4 )

    config_binary=gtk${GTK_VERSION}-unicode-${SLOT}
    alternatives+=( /usr/$(exhost --target)/bin/wx-config /usr/$(exhost --target)/lib/wx/config/${config_binary} )
    edo popd

    # static slot/gtk-ver symlink
    dosym ../lib/wx/config/${config_binary} \
        /usr/$(exhost --target)/bin/wx-config-${SLOT}-gtk${GTK_VERSION}
    # static slot symlink pointing to best gtk version
    dosym ../lib/wx/config/${config_binary} \
        /usr/$(exhost --target)/bin/wx-config-${SLOT}

    alternatives_for wxwidgets ${SLOT}-gtk${GTK_VERSION} ${SLOT} "${alternatives[@]}"
}

install_one_multibuild() {
    default

    wxGTK_handle_alternatives
}

wxGTK_run_phase() {
    echo "  Supported GTK versions: ${SUPPORTED_GTK_VERSIONS[@]}"

    for GTK_VERSION in ${SUPPORTED_GTK_VERSIONS[@]} ; do
        if [[ ${#SUPPORTED_GTK_VERSIONS[@]} -gt 1 ]] && ! option providers:gtk${GTK_VERSION}; then
            echo "  GTK backend version: ${GTK_VERSION} (disabled)"
            continue
        fi

        echo "  GTK backend version: ${GTK_VERSION}"

        if [[ "$(type -t "${EXHERES_PHASE}_prepare_one_multibuild")" == "function" ]] ; then
            "${EXHERES_PHASE}_prepare_one_multibuild"
        fi

        edo pushd "${WORKBASE}/build/gtk${GTK_VERSION}/"

        if [[ "$(type -t "${EXHERES_PHASE}_one_multibuild")" == "function" ]] ; then
            "${EXHERES_PHASE}_one_multibuild"
        else
            default
        fi

        edo popd
    done
}

wxGTK_src_configure() {
    wxGTK_run_phase
}

wxGTK_src_compile() {
    wxGTK_run_phase
}

wxGTK_src_install() {
    wxGTK_run_phase

    # install docs
    option doc && dodoc -r docs/*

    # remove empty dirs
    edo rmdir "${IMAGE}"/usr/share/locale/zh/{LC_MESSAGES,}

    emagicdocs
}

wxGTK_pkg_preinst() {
    local file="${ROOT}"/usr/share/aclocal/wxwin.m4

    if [[ -f ${file} && ! -L ${file} ]]; then
        edo rm ${file}
    fi
}

