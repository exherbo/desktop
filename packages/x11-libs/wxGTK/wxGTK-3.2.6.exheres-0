# Copyright 2009 Maxime Coste <frrrwww@gmail.com>
# Copyright 2014 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require wxGTK

PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    curl [[ description = [ Enable wxWebRequest using libcurl ] ]]
    gstreamer
    joystick
    libnotify [[ description = [ Use libnotify for notification messages ] ]]
    sdl
    secretstore [[ description = [ Enable wxGTK's secretstore using libsecret ] ]]
    tiff
    wayland [[ requires = [ providers: gtk3 ] ]]
    webkit [[ requires = [ providers: gtk3 ] ]]
    X

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-arch/xz
        dev-libs/expat
        dev-libs/glib:2
        dev-libs/libglvnd
        dev-libs/pcre2
        media-libs/fontconfig
        media-libs/libpng:=
        sys-libs/zlib[>=1.1.4]
        x11-dri/glu
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libxkbcommon
        x11-libs/libXpm
        x11-libs/libXtst
        x11-libs/libXxf86vm
        x11-libs/pango [[ note = [ required for unicode support ] ]]
        curl? ( net-misc/curl )
        gstreamer? (
            media-libs/gstreamer:1.0[>=1.7.2]
            media-plugins/gst-plugins-bad:1.0[>=1.7.2]
            media-plugins/gst-plugins-base:1.0[>=1.7.2]
        )
        libnotify? ( x11-libs/libnotify[>=0.7] )
        providers:gtk2? ( x11-libs/gtk+:2[>=2.24] )
        providers:gtk3? ( x11-libs/gtk+:3[>=3.10.2][wayland?] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        sdl? ( media-libs/SDL:2 )
        secretstore? ( dev-libs/libsecret:1 )
        tiff? ( media-libs/tiff:= )
        wayland? ( sys-libs/wayland )
        webkit? ( net-libs/webkit:4.1 )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-3.2.1-avoid-collision.patch
    "${FILES}"/${PN}-3.2.4-defaultlocalesearchpath.patch
)

configure_one_multibuild() {
    econf \
        PKG_CONFIG="/usr/$(exhost --target)/bin/$(exhost --tool-prefix)pkg-config" \
        --hates=docdir \
        --hates=disable-silent-rules \
        --hates=enable-fast-install \
        --enable-compat30 \
        --enable-controls \
        --enable-graphics_ctx \
        --enable-gui \
        --enable-intl \
        --enable-ipv6 \
        --enable-shared \
        --enable-threads \
        --enable-unicode \
        $(if exhost --is-native -q;then
            echo --enable-precomp-headers
        else
            echo --disable-precomp-headers
        fi) \
        --disable-compat28 \
        --disable-gtktest \
        --disable-spellcheck \
        --disable-webviewedge \
        --with-expat \
        --with-gtk=${GTK_VERSION} \
        --with-gtkprint \
        --with-libjpeg=sys \
        --with-liblzma \
        --with-libpng=sys \
        --with-libxpm=sys \
        --with-libiconv \
        --with-nanosvg \
        --with-opengl \
        --with-regex=sys \
        --with-zlib=sys \
        --without-gnomevfs \
        $(option_enable curl webrequest) \
        $(option_enable gstreamer mediactrl) \
        $(option_enable joystick) \
        $(option_enable secretstore) \
        $(option_enable webkit webview) \
        $(option_enable webkit webviewwebkit) \
        $(option_enable !X glcanvasegl) \
        $(option_with libnotify) \
        $(option_with sdl) \
        $(option_with tiff libtiff sys)
}

