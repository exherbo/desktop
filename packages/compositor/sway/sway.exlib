# Copyright 2016 Morgane “Sardem FF7” Glidic <sardemff7@exherbo.org>
# Copyright 2019 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require bash-completion github [ user=swaywm release=${PV} suffix=tar.gz ] \
    meson

SUMMARY="i3-compatible window manager for Wayland"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    fish-completion [[ description = [ Install completion files for the fish shell ] ]]
    gdk-pixbuf [[ description = [ Support more formats for background images ] ]]
    tray [[ description = [ Enable support for swaybar tray ] ]]
    xwayland [[ description = [ Enable XWayland support ( X11 compatibility layer ) ] ]]
    zsh-completion
    ( providers: systemd elogind ) [[
        *description = [ Session tracking provider ]
        number-selected = at-most-one
    ]]
    ( providers: eudev systemd ) [[
        number-selected = exactly-one
    ]]
    tray? (
        ( providers: systemd elogind ) [[
            *note = [ tray requires elogind or systemd support enabled ]
            number-selected = exactly-one
        ]]
    )
"
if ever is_scm; then
    WLROOTS_VERSION="=scm"
else
    WLROOTS_VERSION=">=0.17.0&<0.18.0"
fi

DEPENDENCIES="
    build:
        app-doc/scdoc[>=1.9.2]
        virtual/pkg-config
    build+run:
        dev-libs/json-c:=[>=0.13]
        dev-libs/pcre2
        sys-libs/libinput[>=1.21.0]
        sys-libs/wayland-protocols[>=1.24]
        sys-libs/wayland[>=1.21.0]
        sys-libs/wlroots[xwayland?][${WLROOTS_VERSION}]
        x11-dri/libdrm
        x11-libs/cairo
        x11-libs/libevdev
        x11-libs/libxkbcommon[>=1.5.0]
        x11-libs/pango
        x11-libs/pixman:1
        gdk-pixbuf? ( x11-libs/gdk-pixbuf:2.0 )
        xwayland? (
            x11-libs/libxcb
            x11-utils/xcb-util-wm
        )
        providers:systemd? ( sys-apps/systemd[>=239] )
        providers:elogind? ( sys-auth/elogind[>=239] )
        providers:eudev? ( sys-apps/eudev )
    run:
        wayland-apps/swaybg[gdk-pixbuf?]
    recommendation:
        x11-apps/xkeyboard-config [[ description = [ Support for keyboard layouts and options ] ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddefault-wallpaper=true
    -Dswaybar=true
    -Dswaynag=true
    -Dman-pages=enabled
)

MESON_SRC_CONFIGURE_OPTIONS=(
    'providers:systemd -Dsd-bus-provider=libsystemd'
    'providers:elogind -Dsd-bus-provider=libelogind'
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'gdk-pixbuf gdk-pixbuf'
    'tray'
    'xwayland'
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'bash-completion bash-completions'
    'fish-completion fish-completions'
    'zsh-completion zsh-completions'
)

