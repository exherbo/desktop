# Copyright 2020 Danilo Spinella <danyspin97@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="A seat management daemon and a universal seat management library"
HOMEPAGE="https://git.sr.ht/~kennylevinsen/seatd"
DOWNLOADS="https://git.sr.ht/~kennylevinsen/${PN}/archive/${PV}.tar.gz -> ${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    ( providers: elogind systemd-logind ) [[
        *description = [ Logind support ]
        number-selected = at-most-one
    ]]
"

DEPENDENCIES="
    build:
        app-doc/scdoc[>=1.9.7]
        virtual/pkg-config
    build+run:
        providers:elogind? ( sys-auth/elogind )
        providers:systemd-logind? ( sys-apps/systemd )
"

src_configure() {
    local meson_params=(
        -Dexamples=disabled
        -Dlibseat-builtin=enabled
        -Dlibseat-seatd=enabled
        -Dman-pages=enabled
        -Dserver=enabled
    )

    if option providers:elogind ; then
        meson_params+=( -Dlibseat-logind=elogind )
    elif option providers:systemd-logind ; then
        meson_params+=( -Dlibseat-logind=systemd )
    else
        meson_params+=( -Dlibseat-logind=disabled )
    fi

    exmeson "${meson_params[@]}"
}

