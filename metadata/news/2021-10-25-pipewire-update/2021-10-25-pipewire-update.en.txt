Title: PipeWire 0.3.39 update
Author: Tom Briden <tom@decompile.me.uk>
Content-Type: text/plain
Posted: 2021-10-25
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: media/pipewire[<0.3.39]

As of PipeWire 0.3.39 the default session manager has been changed to
wireplumber as recommended by upstream due to media-session being
considered an example implementation only. If you have any custom pipewire
configs, they may need to be updated. Systemd users may also need to update
user services as so:

# systemctl --user --now disable pipewire-media-session
# systemctl --user --now enable wireplumber

